/**
 * Created by daniel on 2016-03-07.
 */
(function () {
  'use strict';

  var app = angular.module('LearnApp.Common');

  app.constant('httpUrls', {
    dev: true,
    restBaseUrlDev: 'http://localhost:3000',
    restBaseUrl: 'http://localhost:3000',
    getBaseUrl: function () {
      if (this.dev) {
        return this.restBaseUrlDev
      } else {
        return this.restBaseUrl
      }
    }
  })

})();
