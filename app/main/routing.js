/**
 * Created by daniel on 2016-03-07.
 */
(function () {
  'use strict';

  var app = angular.module('LearnApp');

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/topics");
    $stateProvider
      .state('topics', {
        url: "/topics",
        templateUrl: 'app/topics/partials/list.html',
        controller: 'LearnApp.Topics.List.Ctrl'
      })

  }])

})();
