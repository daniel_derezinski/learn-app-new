/**
 * Created by daniel on 2016-03-07.
 */
(function () {
  'use strict';

  var app = angular.module("LearnApp.Rest");

  app.factory('Topics', ['topics.list',
    function (list) {
      return {
        list: list
      }
    }]);

  app.factory('errorCallback', ['$log', function ($log) {
    return function (response, message) {
      $log.error("error " + message, response);
    }
  }]);


  app.factory('topics.list', ['$http', "errorCallback", 'httpUrls',
    function ($http, errorCallback, httpUrls) {
      var url = httpUrls.getBaseUrl() + '/topics';

      return function (sCallback, eCallback) {
        eCallback = eCallback || errorCallback;

        $http.get(url).then(sCallback, function (response) {
          eCallback(response, 'in topics list');
        });

      }
    }])


})();
