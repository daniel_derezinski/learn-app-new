/**
 * Created by daniel on 2016-03-07.
 */
(function () {
  'use strict';

  var app = angular.module('LearnApp.Topics');

  app.controller('LearnApp.Topics.List.Ctrl', ['$scope', 'Topics', function ($scope, Topics) {

    $scope.topicsList = [];

    Topics.list(function (response) {
      $scope.topicsList = response.data
    })

  }])

})();
